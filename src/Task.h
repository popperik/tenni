#pragma once

#include <ctime>
#include <string>
#include <iostream>
using namespace std;

#include "Date.h"

class Task {
    string title = "";
    bool completed = false;
    Date dueDate;

    string uid;
public:
    Task();

    bool operator==(const Task& other) const;
    bool operator!=(const Task& other) const;

    const Date& getDueDate() const;
    string getTitle() const;
    bool isCompleted() const;
    string getUid() const;

    void setCompleted(bool completed);
    void setDueDate(const Date& dueDate);
    void setTitle(string title);
    void setUid(string uid);

    string toString() const;

    friend ostream& operator<<(ostream& stream, const Task& task);
    friend istream& operator>>(istream& stream, Task& task);
};
