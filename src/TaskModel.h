#include <QObject>

#include "Task.h"

#pragma once

class TaskModel : public QObject, public Task {
    Q_OBJECT
    Q_PROPERTY(QString title READ getTitle)
public:
    QString getTitle() const;
};
