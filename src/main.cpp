#include <QGuiApplication>
#include <QQmlContext>
#include <QQuickView>

#include "TaskModel.h"

int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    TaskModel task;
    task.setTitle("shopping");

    QGuiApplication app(argc, argv);
    QQuickView view;
    view.setSource(QUrl("qrc:/main.qml"));
    view.rootContext()->setContextProperty("task", &task);
    view.show();
    return app.exec();
}
