#include "Task.h"

Task::Task() {}

bool Task::operator==(const Task& other) const {
    return title == other.title;
}

bool Task::operator!=(const Task& other) const {
    return !operator==(other);
}

const Date& Task::getDueDate() const {
    return dueDate;
}

string Task::getTitle() const {
    return title;
}

bool Task::isCompleted() const {
    return completed;
}

string Task::getUid() const {
    return uid;
}

void Task::setCompleted(bool completed) {
    this->completed = completed;
}

void Task::setDueDate(const Date& dueDate) {
    this->dueDate = dueDate;
}

void Task::setTitle(string title) {
    this->title = title;
}

void Task::setUid(string uid) {
    this->uid = uid;
}

string Task::toString() const {
    string box = completed ? "[X]" : "[ ]";
    return box + " " + title;
}

ostream& operator<<(ostream& stream, const Task& task) {
    return stream
        << task.uid << endl
        << task.title << endl
        << task.completed << ' '
        << task.dueDate;
}

istream& operator>>(istream& stream, Task& task) {
    while (stream.peek() == '\n' || stream.peek() == '\r') {
        stream.ignore(1);
    }
    if (!stream) {
        return stream;
    }

    getline(stream, task.uid);
    getline(stream, task.title);
    return stream >> task.completed >> task.dueDate;
}
