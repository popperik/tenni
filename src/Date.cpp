#include "Date.h"

Date::Date() {
    time_t t = time(nullptr);
    tm* tm = localtime(&t);
    setYear(tm->tm_year + 1900);
    setMonth(tm->tm_mon + 1);
    setDay(tm->tm_mday);
}

Date::Date(int year, int month, int day) {
    setYear(year);
    setMonth(month);
    setDay(day);
}

int Date::getDay() const {
    return day;
}

int Date::getMonth() const {
    return month;
}

int Date::getYear() const {
    return year;
}

void Date::setDay(int day) {
    if (day < 0 || day > 31) {
        throw "Invalid day";
    }

    this->day = day;
}

void Date::setMonth(int month) {
    if (month < 0 || month > 12) {
        throw "Invalid month";
    }

    this->month = month;
}

void Date::setYear(int year) {
    this->year = year;
}

string Date::toString() const {
    return to_string(year) + '.' + to_string(month) + '.' + to_string(day);
}

ostream& operator<<(ostream& stream, const Date& date) {
    return stream << date.getYear() << ' ' << date.getMonth() << ' ' << date.getDay();
}

istream& operator>>(istream& stream, Date& date) {
    int year, month, day;
    stream >> year >> month >> day;
    date.setYear(year);
    date.setMonth(month);
    date.setDay(day);
    return stream;
}
