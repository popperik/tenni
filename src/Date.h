#pragma once

#include <ctime>
#include <iostream>
using namespace std;

class Date {
    int year;
    int month;
    int day;

public:
    Date();
    Date(int year, int month, int day);

    int getDay() const;
    int getMonth() const;
    int getYear() const;

    void setDay(int day);
    void setMonth(int month);
    void setYear(int year);

    string toString() const;
};

ostream& operator<<(ostream& stream, const Date& date);
istream& operator>>(istream& stream, Date& date);
