import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.11

Rectangle {
    ColumnLayout {
        Repeater {
            model: [task, task]
            Label {
                text: modelData.title
            }
        }
    }
}
